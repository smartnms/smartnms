<?php
/**
 * @file
 * Contains \Drupal\example\Controller\ExampleController.
 */
namespace Drupal\smartnms\Controller;
use Drupal\Core\Controller\ControllerBase;

class smartnmsController {
	public function configAction() {
		return [
			'#markup' => '<h2>Welcome to the config page</h2>',
		];
	}
	public function settings() {
		return [
			'#markup' => '<h2>Página de gestión de la plataforma Smart NMS</h2>',
		];
	}
}
